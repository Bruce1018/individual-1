# Individual project 1

> author: Zilin Xu  
> netID: zx112

# Demo picture
![](/lib/截屏2024-03-10%2020.08.48.png)
# Demo video
[click here](https://www.youtube.com/watch?v=hrHK9mUlg8c)

# Project URL
[AWS S3 Bucket](http://individual-proj.s3-website-us-east-1.amazonaws.com/)
[Gitlab URL](https://individual-1-bruce1018-1752e02a7a6b797aafd620f44fc5424934688ffb.gitlab.io/)

# Requirement 1: Website built with Zola

Here is the directory of my `Zola` project

![](lib/1.png)

It looks similar to `mini project 1`. The `_index.md` file is the root file and the remain `.md` files are posts on the website

# Requirement 2: GitLab workflow to build and deploy site on push

With the `.yml` file, my project successfully fit this requirement. Here are pictures.

![](/lib/截屏2024-03-10%2020.04.23.png)

![](/lib/截屏2024-03-10%2020.04.32.png)

# Requirement 3: Hosted on AWS S3

Similar to `week3 mini project`. Create a new `S3 Bucket` on the **AWS**

Edit all the changes needed to make the website **Public**

Run the following command `aws s3 sync ./public s3://individual-proj --acl public-read`

Below are some important settings

![](/lib/截屏2024-03-10%2020.07.52.png)

