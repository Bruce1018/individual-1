+++
paginate_by = 10
path = "posts"
title = "Articles"
sort_by = "date"

[extra]
desc = {title = "Hello! I am Bruce", img = "/bruce-img.JPG", text = "This page serves for IDS721 at Duke in Spring 2024."}
+++
